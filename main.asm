%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define KEYSIZE 8

global _start

section .rodata
  overflow_msg: db 'Word is too large for the buffer', '\n', 0
  not_found_msg: db 'Word not found in the dictionary', '\n', 0

section .bss
buffer: resb BUFFER_SIZE

section .text

_start:     
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_line

    test rax, rax 
    jz .word_overflow

    push rdx
    mov rdi, rax
    mov rsi, DICT
    call find_word

    test rax, rax
    jz .not_found       

    mov rdi, rax
    add rdi, KEYSIZE
    pop rdx
    lea rdi, [rax+8+rdx+1]
    call print_string

    jmp .end

    .word_overflow:
        mov rdi, overflow_msg
        call print_string
        xor rdi, rdi
        call exit

    .not_found:
        mov rdi, not_found_msg
        
    .enderr:
        call print_error
        xor rdi, rdi
        call exit

    .end:
        call print_string
        xor rdi, rdi
        call exit
