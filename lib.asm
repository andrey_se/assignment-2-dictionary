global exit
global string_length
global print_string
global print_error 
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_line

global parse_uint
global parse_int
global string_copy


section .data
    space   db 0x20, 0x0A, '\t'  ; Пробельные символы
    
    %define SYS_READ  0
    %define SYS_WRITE 1
    %define SYS_EXIT  60

    ; File descriptors
    %define STDIN     0
    %define STDOUT    1
    %define STDERR    2

    %define BEGIN_STROKE 1
    %define LEN_STROKE 1
    %define NEW_LINE 0xA

    error_msg db "Error occurred.", 0   ; сообщение об ошибке

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .length_loop:
        inc rax
        cmp byte[rdi+rax-1], 0
        jne .length_loop
    dec rax
    ret
    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi

    mov rdx, rax    ; записали длину строки
    mov rax, 1  ; syscall для вывода в stdout
    mov rdi, 1  ; передаём адрес начала строки в stdout
    syscall
    ret


print_error:
    push rdi
    call string_length
    pop rsi 
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDERR
    mov rsi, error_msg
    syscall
    ret




; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, STDOUT  ; syscall для вывода в stdout
    mov rdi, BEGIN_STROKE  ; передаём адрес начала строки в stdout
    mov rdx, LEN_STROKE  ; длина строки (1 символ)
    mov  rsi, rsp
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 1
    mov r10, NEW_LINE
    mov rax, rdi
    dec rsp
    mov byte [rsp], 0
    .print_loop:
        xor rdx, rdx
        div r10
        add dl, '0'
        inc rcx
        dec rsp
        mov byte [rsp], dl
        test rax, rax
        jne .print_loop
    .end:
        mov rdi, rsp
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .negative
    call print_uint
    ret

    .negative:
        ; Если число отрицательное, выведем минус и инвертируем его
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    mov rax, 1
    .equal_loop:
        mov al, byte [rdi]
        cmp al, byte [rsi]
        jne .notEqual
        test al, al
        je .equal
        inc rdi
        inc rsi
        jmp .equal_loop
    .equal:
        mov rax, 1
        ret
    .notEqual:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:    
    push 0
    mov rsi, rsp        ; указатель на буфер для прочитанных данных
    mov rdx, 1          ; количество байт для чтения (в данном случае, один символ)
    mov rax, 0          ; syscall для чтения
    mov rdi, 0          ; файловый дескриптор stdin
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13
    push r14
    push r15
    push rdi
    mov r13, rdi
    mov r14, rsi
    xor r15, r15
    dec r14  

    .skip_whitespace:
        ; Читаем один символ из stdin
        call read_char
        cmp al, ` `
        je .skip_whitespace
        cmp al, `\t`
        je .skip_whitespace
        cmp al, `\n`
        je .skip_whitespace
        test al, al
        je .word_done

    .read_into_buffer:
        test r14, r14
        jle .buffer_full
        mov byte [r13], al
        inc r13        
        dec r14          
        inc r15
        call read_char
        cmp al, byte [space]
        je .word_done
        test rax, rax
        je .word_done
        jmp .read_into_buffer
    .word_done:
        pop rax
        mov byte [r13], byte 0
        mov rdx, r15
        pop r15
        pop r14
        pop r13
        ret
    .buffer_full:
        pop rax
        xor rax, rax
        pop r15
        pop r14
        pop r13
        ret

read_line:
    push r13
    mov r13, rdi
    push r14
    mov r14, rdi
    push r15
    mov r15, rsi
    
    .check_spaces:
        call read_char
        cmp al, '\t'    ; Табуляция (0x09)
        je .check_spaces
        cmp al, '\n'    ; Перевод строки (0x0A)
        je .check_spaces
        cmp al, ' '     ; Пробел
        je .check_spaces

        
    .read_w:                                
        test rax, rax
        je .success
        cmp al, 0x9
        je .success
        cmp al, 0xA
        je .success
        
        dec r15
        test r15, r15
        jz .fail
        mov byte[r14], al
        inc r14
        call read_char
        jmp .read_w
        
    .success:
        mov byte[r14], 0
        sub r14, r13  
        mov rdx, r14
        mov rax, r13
        jmp .done
        
    .fail:
        xor rax, rax
        jmp .done
        
    .done:
        pop r13
        pop r14
        pop r15
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx: его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 10
    xor r9, r9
    xor rax, rax
    mov rcx, -1
    xor rdx, rdx
    
    .reading_char:
        inc rcx
        mov r9b, byte [rdi+rcx]
        test r9b, r9b
        je .end
        cmp r9b, '0'
        jl .end
        cmp r9b, '9'
        jg .end
        sub r9b, '0'
        mul r8
        add rax, r9
        jmp .reading_char
    
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    jmp parse_uint
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    test rdx, rdx
    mov rax, rsi
    je .buffer_full
    mov r8b, byte [rdi]
    mov byte [rsi], r8b
    test r8b, r8b
    je .end
    dec rdx
    inc rsi
    inc rdi
    jmp string_copy
    .end:
        sub rsi, rax
        mov rax, rsi
        ret
    .buffer_full:
        xor rax, rax
        ret