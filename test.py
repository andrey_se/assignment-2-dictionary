import subprocess

class TestResult:
    def __init__(self, input_data, success, expected_output, expected_errors, received_output, received_errors):
        self.input_data = input_data
        self.success = success
        self.expected_output = expected_output
        self.expected_errors = expected_errors
        self.received_output = received_output
        self.received_errors = received_errors

    def __str__(self):
        if self.success:
            return f"input: {self.input_data}, success: {self.success}"
        return f"input: {self.input_data}, success: {self.success}\nexpected output: {self.expected_output}, " \
            f"expected errors: {self.expected_errors}, received output {self.received_output}, " \
            f"received errors: {self.received_errors}"

def run_test(main, input_data):
    process = subprocess.Popen(main, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_data, stderr_data = process.communicate(input_data.encode())
    stdout_data = stdout_data.decode().strip()
    stderr_data = stderr_data.decode().strip()
    process.kill()
    return stdout_data, stderr_data

tests = [
    ("one", "1", ""),
    ("three", "3", ""),
    ("four", "4", ""),
    ("a"*256, "", "word too long"),
    ("abc", "", "no such element"),
    ("fifth word", "five", ""),
    ("one two", "", "no such element"),
]

successfully_passed_test = 0
for i, (input_data, expected_output, expected_errors) in enumerate(tests, start=1):
    received_output, received_errors = run_test(["./main"], input_data)
    success = received_errors == expected_errors and received_output == expected_output
    result = TestResult(input_data, success, expected_output, expected_errors, received_output, received_errors)
    if result.success:
        successfully_passed_test += 1
    print(f"test {i}: {result}")
    print("-------------------------------------------------")

print(f"successfully passed {successfully_passed_test} of {len(tests)} tests")
