ASM=nasm
ASMFLAGS=-felf64
LD=ld
PY=python3

SRCS=$(wildcard *.asm)
OBJS=$(patsubst %.asm,%.o,$(SRCS))

%.o: %.asm words.inc
	$(ASM) $(ASMFLAGS) $< -o $@

main: main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm -rf *.o 2>/dev/null
	rm -f main 2>/dev/null

.PHONY: test
test:
	$(PY) test.py