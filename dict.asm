%define SIZE_OF_POINTER 8

section .text
	extern string_equals
    global find_word

find_word:
    ; Проверка на нулевые указатели
    test rdi, rdi
    je .dict_exit
    test rsi, rsi
    je .dict_exit

    ; Сохранение регистра rsi (первый указатель)
    push rsi

    .loop:
        ; Переход к следующему указателю
        lea rsi, [rsi + SIZE_OF_POINTER]
        call string_equals

        ; Проверка результата сравнения
        test rax, rax
        jnz .access

        ; Загрузка следующего указателя
        mov rsi, [rsi]

        ; Проверка на нулевой указатель
        test rsi, rsi
        jnz .loop

    .dict_exit:
        xor rax, rax
        pop rsi  ; Восстановление регистра rsi
        ret

    .access:
        pop rsi  ; Восстановление регистра rsi
        mov rax, rsi
        ret
